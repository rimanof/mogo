$(document).ready(function () {
    var header = $("#header"),
        introH = $("#intro").innerHeight(),
        scrollOffset = $(window).scrollTop();

    /*fixed Header*/
    checkScroll();
    $(window).scroll(function () {
        scrollOffset = $(this).scrollTop();
        checkScroll();
    })

    function checkScroll() {
        if (scrollOffset >= introH) {
            header.addClass("active");
        } else {
            header.removeClass("active");
            $(".nav").removeClass("active");
            $(".nav-toggle").removeClass("active");
        }
    }

    /*Smoth Scroll*/
    $("[data-scroll]").on("click", function (event) {
        event.preventDefault();
        var $this = $(this),
            blockId = $(this).data("scroll"),
            blockOffset = $(blockId).offset().top;

        $(".activ").removeClass("activ");
        $this.addClass("activ");

        $("html,body").animate({
            scrollTop: blockOffset
        });
    })

    /*Menu nav toggle*/
    $(".nav-toggle").click(function (event) {
        event.preventDefault();
        $(".nav-toggle").toggleClass("active");
        $(".nav").toggleClass("active");
        if ($('.header').hasClass("active")) {
            checkScroll();
        } else {
            $(".header").toggleClass("active");
        }
    })

    /*toggle accordion*/
    $("[data-collapse]").on("click", function (event) {
        event.preventDefault();
        var $this = $(this),
            blockId = $(this).data("collapse");
        $(blockId).slideToggle();
        $(this).toggleClass("active");
    })

    /*slider*/
    $(".data-slider").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });
})